from nltk.tag import StanfordNERTagger
from nltk.internals import find_jars_within_path  # to solve the problem with java classpath
import os


class NerTaggerWrapper(object):
    tagger = None

    def __init__(self):
        root = os.path.dirname(os.path.abspath(__file__)) + '/bin/'
        self.tagger = StanfordNERTagger(root + 'NER/classifiers/english.all.3class.distsim.crf.ser.gz',root + 'NER/stanford-ner.jar',encoding='utf-8')
        st_jar_type = type(self.tagger._stanford_jar)
        if st_jar_type != list and st_jar_type != tuple:
            stanford_dir = self.tagger._stanford_jar.rpartition('/')[0]
            stanford_jars = find_jars_within_path(stanford_dir)
            self.tagger._stanford_jar = ':'.join(stanford_jars)

    def tag(self, tokens):
        return self.tagger.tag(tokens)

